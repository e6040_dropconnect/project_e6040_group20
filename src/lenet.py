"""
This code is based on source code for Homework 3.b of ECBM E6040, Spring 2016, Columbia University

Instructor: Prof. Aurel A. Lazar

"""
import numpy

import theano
import theano.tensor as T
from theano.tensor.signal import downsample

from utils import shared_dataset, load_data
from nn import LogisticRegression, LeNetConvPoolLayer, train_nn, DropoutLayer, DropConnectLayer, HiddenLayer
import sys
import timeit
import inspect
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import matplotlib.pyplot as plt
plt.switch_backend('agg')


def test_lenet(learning_rate=0.01, n_epochs=1000, nkerns=[16, 512], filterShapes=[(5,5),(5,5)], poolSize=(2,2), p=0.5,
            batch_size=128, verbose=False, method='dropconnect', d=500, initial_momentum = 0.5, plots=False, plot_type=3):
    """
    Wrapper function for testing LeNet on SVHN dataset

    :type learning_rate: float
    :param learning_rate: learning rate used (factor for the stochastic
    gradient)

    :type n_epochs: int
    :param n_epochs: maximal number of epochs to run the optimizer

    :type nkerns: list of ints
    :param nkerns: number of kernels on each layer

    :type filtershapes: list of int tuples
    :param nkerns: number of filters for each convolutional layer

    :type poolSize: int tuple
    :param nkerns: Pooling factor for each layer of convolutional layer

    :type batch_size: int
    :param batch_szie: number of examples in minibatch.

    :type p: float
    :param p: Probability for dropout or dropconnect

    :type method: string - 'dropconnect', 'dropout' or 'nodrop'
    :param method: Regularization method to use

    :type initial_momentum: float between 0 and 1
    :param initial_momentum: Initial value of momentum

    :type plot_type: int - 1, 2 or 3
    :param plot_type: The kind of plot to use

    :type verbose: boolean
    :param verbose: to print out epoch summary or not to.

    """
    seed = 23455
    rng = numpy.random.RandomState(seed)
    initial_momentum = initial_momentum

    datasets = load_data()

    train_set_x, train_set_y = datasets[0]
    valid_set_x, valid_set_y = datasets[1]
    test_set_x, test_set_y = datasets[2]

    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    n_valid_batches //= batch_size
    n_test_batches //= batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    # Dropout and Dropconnect forward pass depends on whether it is training or testing. 
    is_train = T.iscalar('is_train')

    # Create a shared value with initial value of momentum
    momentum =theano.shared(numpy.cast[theano.config.floatX](initial_momentum), name='momentum')
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # Reshape matrix of rasterized images of shape (batch_size, 3 * 32 * 32)
    # to a 4D tensor, compatible with our LeNetConvPoolLayer
    layer0_input = x.reshape((batch_size, 3, 32, 32))

    #Construct the first convolutional pooling layer
    layer0 = LeNetConvPoolLayer(
       rng,
       input=layer0_input,
       image_shape=(batch_size, 3, 32, 32),
       filter_shape=(nkerns[0], 3, filterShapes[0][0], filterShapes[0][1]),
       poolsize=poolSize
    )
    img_shape_2=(
        (32-filterShapes[0][0]+1)/poolSize[0],
        (32-filterShapes[0][1]+1)/poolSize[1]
    )
    #Construct the second convolutional pooling layer
    layer1 = LeNetConvPoolLayer(
       rng,
       input=layer0.output,
       image_shape=(batch_size, nkerns[0], img_shape_2[0], img_shape_2[1]),
       filter_shape=(nkerns[1], nkerns[0], filterShapes[1][0], filterShapes[1][1]),
       poolsize=poolSize
    )

    # the HiddenLayer being fully-connected, it operates on 2D matrices of
    # shape (batch_size, num_pixels) (i.e matrix of rasterized images).
    layer2_input = layer1.output.flatten(2)
    
    img_shape_3=(
        (img_shape_2[0]-filterShapes[1][0]+1)/poolSize[0],
        (img_shape_2[1]-filterShapes[1][1]+1)/poolSize[1],
    )
    
    

    
    d = d
    # Different layers based on whether we need dropout, dropconnect or regular hidden layer
    if method is 'dropconnect':
        layer2 = DropConnectLayer(
             p=p,
             rng=rng,
             input=layer2_input,
             is_train=is_train,
             n_in=nkerns[1]*img_shape_3[0]*img_shape_3[1],
             n_out= d,
             seed=seed,
             batch_size=batch_size,
             activation=T.tanh
         )
    elif method is 'dropout':
        layer2 = DropoutLayer(
             p=p,
             rng=rng,
             input=layer2_input,
             is_train=is_train,
             n_in=nkerns[1]*img_shape_3[0]*img_shape_3[1],
             n_out= d,
             seed=seed,
             batch_size=batch_size,
             activation=T.tanh
         )
    elif method is 'nodrop':
        #print('Using nodrop')
        layer2 = HiddenLayer(
             rng,
             input=layer2_input,
             n_in=nkerns[1] * img_shape_3[0]*img_shape_3[1],
             n_out=d,
             activation=T.tanh,
        )

    #classify the values of the fully-connected sigmoidal layer
    layer3 = LogisticRegression(input=layer2.output, n_in=d, n_out=10)
    
    # the cost we minimize during training is the NLL of the model
    cost = layer3.negative_log_likelihood(y)

    # create a function to compute the mistakes that are made by the model
    
    if method is 'dropconnect' or method is 'dropout':
        test_model = theano.function(
            [index],
            [layer3.errors(y),layer3.negative_log_likelihood(y)],
            givens={
                x: test_set_x[index * batch_size: (index + 1) * batch_size],
                y: test_set_y[index * batch_size: (index + 1) * batch_size],
                is_train: numpy.cast['int32'](0)
            }
        )
        validate_model = theano.function(
            [index],
            layer3.errors(y),
            givens={
                x: valid_set_x[index * batch_size: (index + 1) * batch_size],
                y: valid_set_y[index * batch_size: (index + 1) * batch_size],
                is_train: numpy.cast['int32'](0)
            }
        )
    elif method is 'nodrop':
        test_model = theano.function(
            [index],
            [layer3.errors(y),layer3.negative_log_likelihood(y)],
            givens={
                x: test_set_x[index * batch_size: (index + 1) * batch_size],
                y: test_set_y[index * batch_size: (index + 1) * batch_size]
            }
        )
        validate_model = theano.function(
            [index],
            layer3.errors(y),
            givens={
                x: valid_set_x[index * batch_size: (index + 1) * batch_size],
                y: valid_set_y[index * batch_size: (index + 1) * batch_size]
            }
        )

    
    
     
    

    #Create a list of all model parameters to be fit by gradient descent
    params = layer3.params + layer2.params + layer1.params + layer0.params
    #dropParams = [layer2.params[0]]+[layer2.params[2]]
    # create a list of gradients for all model parameters
    #grads = T.grad(cost, params, consider_constant=layer2.consider_constant)
    # gradsDrop = T.grad(cost, dropParams)
    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    #updates = [
    #    (param_i, param_i - learning_rate * grad_i)
    #    for param_i, grad_i in zip(params, grads)
    #]

    # This mask value would always be in sync in value generated within Dropconnect layer. We need this mask so that only those weights are upgrated during backprop that were active for example
    srng = RandomStreams(seed=seed)
    mask = srng.binomial(n=1, p=p, size=(nkerns[1]*img_shape_3[0]*img_shape_3[1],d), dtype=theano.config.floatX)

    if method is 'nodrop':
        updates = []
        for param in params:
            param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
            updates.append((param, param - learning_rate*param_update))
            updates.append((param_update, momentum*param_update + (1. - momentum)*T.grad(cost, param)))
            
        train_model = theano.function(
            [index],
            cost,
            updates=updates,
            givens={
                x: train_set_x[index * batch_size: (index + 1) * batch_size],
                y: train_set_y[index * batch_size: (index + 1) * batch_size]
            }
        )
    elif method is 'dropconnect':
        updates = []
        for param in params:
            param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
            updates.append((param, param - learning_rate*param_update))
            updates.append((param_update, momentum*param_update + (1. - momentum)*mask*T.grad(cost, param, consider_constant=layer2.consider_constant)))
            
            
        train_model = theano.function(
            [index],
            cost,
            updates=updates,
            givens={
                x: train_set_x[index * batch_size: (index + 1) * batch_size],
                y: train_set_y[index * batch_size: (index + 1) * batch_size],
                is_train: numpy.cast['int32'](1)
            }
        )
    elif method is 'dropout':
        updates = []
        for param in params:
            param_update = theano.shared(param.get_value()*0., broadcastable=param.broadcastable)
            updates.append((param, param - learning_rate*param_update))
            updates.append((param_update, momentum*param_update + (1. - momentum)*T.grad(cost, param, consider_constant=layer2.consider_constant)))
            
            
        train_model = theano.function(
            [index],
            cost,
            updates=updates,
            givens={
                x: train_set_x[index * batch_size: (index + 1) * batch_size],
                y: train_set_y[index * batch_size: (index + 1) * batch_size],
                is_train: numpy.cast['int32'](1)
            }
        )

    ###############
    # TRAIN MODEL #
    ###############
    print('... training')

    # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    validation_frequency = min(n_train_batches, patience // 2)
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    entropy_tr = []
    entropy_te = []
    
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        print "epoch: ", epoch
        ent_train = []
        ent_test = []
        for minibatch_index in range(n_train_batches):
        
            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
                
            cost_ij = train_model(minibatch_index)
            ent_train.append(cost_ij)
            
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)
                
                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean([x[0] for x in test_losses])
                    
                    ent_test.append(numpy.mean([x[1] for x in test_losses]))

                    if verbose:
                        print(('epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

        entropy_tr.append(numpy.mean(ent_train))
        entropy_te.append(numpy.mean(ent_test))
        
        # Increase the value of momentum with each epoch
        if momentum.get_value() < 0.99:
            new_momentum = 1. - (1. - momentum.get_value()) * 0.98
            momentum.set_value(numpy.cast[theano.config.floatX](new_momentum))

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    #curframe = inspect.currentframe()
    #calframe = inspect.getouterframes(curframe, 2)

    # Print out summary
    print('Optimization complete.')
    print('Best validation score of %f %% obtained at iteration %i, with test performance %f %%' % (best_validation_loss * 100., best_iter + 1, test_score * 100.))
    print(('The training process for method ' + method + ' ran for %.2fm' % ((end_time - start_time) / 60.)))
    
    if plots:
        
        if plot_type==1 or plot_type==2:
            return test_score * 100.
        
        if plot_type==3:
            return (epoch,entropy_tr,entropy_te)  

def plotEntropy(learning_rate=0.01, n_epochs=100, nkerns=[64, 64], filterShapes=[(5,5),(5,5)], poolSize=(2,2),
            batch_size=128, verbose=False, initial_momentum = 0.5):
    # Plot entropy of the three techniques against the number of epochs
    
    print "Training using DropConnect"
    (epoch_dropc,entropy_dropc_tr,entropy_dropc_te) = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, nkerns=nkerns,
                                           filterShapes=filterShapes, poolSize=poolSize, 
                                           batch_size=batch_size, verbose=verbose, method='dropconnect',
                                           initial_momentum=initial_momentum, plots=True, plot_type=3)
    print "Training using no drop"
    (epoch_ndrop,entropy_ndrop_tr,entropy_ndrop_te) = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, nkerns=nkerns,
                                           filterShapes=filterShapes, poolSize=poolSize, 
                                           batch_size=batch_size, verbose=verbose, method='nodrop',
                                           initial_momentum=initial_momentum, plots=True, plot_type=3)
    
    print "Training using Dropout"
    (epoch_dropo,entropy_dropo_tr,entropy_dropo_te) = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, nkerns=nkerns,
                                           filterShapes=filterShapes, poolSize=poolSize, 
                                           batch_size=batch_size, verbose=verbose, method='dropout',
                                           initial_momentum=initial_momentum, plots=True, plot_type=3)
    
    plt.figure(1)
    plt.plot(numpy.arange(1,epoch_ndrop+1),entropy_ndrop_tr,label='No-drop Train', linestyle='-', color='r', marker='+')
    plt.plot(numpy.arange(1,epoch_ndrop+1),entropy_ndrop_te,label='No-drop Test', linestyle='--', color='r')
    plt.plot(numpy.arange(1,epoch_dropc+1),entropy_dropc_tr,label='DropConnect Train', linestyle='-', color='b', marker='+')
    plt.plot(numpy.arange(1,epoch_dropc+1),entropy_dropc_te,label='DropConnect Test', linestyle='--', color='b')
    plt.plot(numpy.arange(1,epoch_dropo),entropy_dropo_tr,label='Dropout Train', linestyle='-', color='g', marker='+')
    plt.plot(numpy.arange(1,epoch_dropo),entropy_dropo_te,label='Dropout Test', linestyle='--', color='g')
    
    plt.xlabel('Epochs')
    plt.ylabel('Cross entropy')
    plt.legend()
    plt.show()
    

def plotOverUnits(learning_rate=0.01, n_epochs=100, nkerns=[64, 64], filterShapes=[(5,5),(5,5)], poolSize=(2,2),
            batch_size=128, verbose=False, initial_momentum = 0.5):
    # Plot peformance of three techniqes by varying the number of hidden units. 
    error_dropc_te = []
    error_ndrop_te = []
    error_dropo_te = []
    
    for d in [200,400,800,1000]:
        err_dropc = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, 
                         nkerns=nkerns, filterShapes=filterShapes, poolSize=poolSize, 
                         batch_size=batch_size, verbose=verbose, method='dropconnect',
                         d=d, initial_momentum=initial_momentum, plots=True, plot_type=1)
        err_ndrop = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, 
                         nkerns=nkerns, filterShapes=filterShapes, poolSize=poolSize, 
                         batch_size=batch_size, verbose=verbose, method='nodrop',
                         d=d, initial_momentum=initial_momentum, plots=True, plot_type=1)
        err_dropo = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, 
                         nkerns=nkerns, filterShapes=filterShapes, poolSize=poolSize, 
                         batch_size=batch_size, verbose=verbose, method='dropout',
                         d=d, initial_momentum=initial_momentum, plots=True, plot_type=1)
        
        error_dropc_te.append(err_dropc)
        error_ndrop_te.append(err_ndrop)
        error_dropo_te.append(err_dropo)
        
    plt.figure(1)
    plt.plot([200,400,800,1000],err_dropc,label='DropConnect', linestyle='-', color='r')
    plt.plot([200,400,800,1000],err_ndrop,label='No-drop', linestyle='-', color='b')
    plt.plot([200,400,800,1000],err_dropo,label='Dropout', linestyle='-', color='g')
    plt.xlabel('Hidden units')
    plt.ylabel('Test Error (%)')
    plt.legend()
    plt.show()
    
def plotOverP(learning_rate=0.01, n_epochs=100, nkerns=[64, 64], filterShapes=[(5,5),(5,5)], poolSize=(2,2),
            batch_size=128, verbose=False, initial_momentum = 0.5):
    # Plot the performance of dropout and dropconnect for varying values of probability
    error_dropc_te = []
    error_dropo_te = []
    probs = [0.1,0.3,0.5,0.7,0.9]
    
    for p in probs:
        err_dropc = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, p=p, 
                         nkerns=nkerns, filterShapes=filterShapes, poolSize=poolSize, 
                         batch_size=batch_size, verbose=verbose, method='dropconnect',
                         d=d, initial_momentum=initial_momentum, plots=True, plot_type=1)
        err_dropo = test_lenet(learning_rate=learning_rate, n_epochs=n_epochs, p=p,
                         nkerns=nkerns, filterShapes=filterShapes, poolSize=poolSize, 
                         batch_size=batch_size, verbose=verbose, method='dropout',
                         d=d, initial_momentum=initial_momentum, plots=True, plot_type=1)
        
        error_dropc_te.append(err_dropc)
        error_dropo_te.append(err_dropo)
        
    plt.figure(1)
    plt.plot(probs,err_dropc,label='DropConnect', linestyle='-', color='r')
    plt.plot(probs,err_dropo,label='Dropout', linestyle='-', color='g')
    plt.xlabel('% of Elements Kept')
    plt.ylabel('Test Error (%)')
    plt.legend()
    plt.show()



if __name__ == "__main__":
    test_lenet(nkerns=[5, 10], verbose=True, method='dropout')